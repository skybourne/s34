const express = require('express')

const app = express()

const port = 3000

app.use(express.json())

app.use(express.urlencoded({extended:true}))

// Routes

//home route
app.get('/home' , (request, response)=>{
	response.send('Welcome to the home page')
})

//get users
let users = [
{
	"username" : "johndoe",
	"password" : "johndoe1234"
}

]

app.get('/users', (request, response) => {
	response.send(users)
})

// Delete users
let message;
app.delete('/delete-user', (request, response) => {
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			message = `User ${users[i].username} has been deleted!`
			
			users.splice(i, 1)
			break
		} else {
			message = `You are not registered yet!`
		}
	}
	response.send(message)
})




// Listen
app.listen(port, ()=> console.log(`Server is running at localhost: ${port}`))



